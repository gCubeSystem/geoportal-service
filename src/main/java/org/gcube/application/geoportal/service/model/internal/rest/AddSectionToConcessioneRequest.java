package org.gcube.application.geoportal.service.model.internal.rest;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlRootElement;

import org.gcube.application.geoportal.model.concessioni.LayerConcessione;
import org.gcube.application.geoportal.model.concessioni.RelazioneScavo;
import org.gcube.application.geoportal.model.content.AssociatedContent;
import org.gcube.application.geoportal.model.content.OtherContent;
import org.gcube.application.geoportal.model.content.UploadedImage;
import org.gcube.application.geoportal.model.gis.SDILayerDescriptor;

import lombok.Data;

@XmlRootElement
@Data
public class AddSectionToConcessioneRequest {

	public static enum Section{
		RELAZIONE,UPLOADED_IMG,PIANTA,POSIZIONAMENTO,OTHER
	}
	
	@XmlRootElement
	@Data
	public static class SHUBFileDescriptor {
		private String filename;
		private String shubID;
	}
	
	
	private Section section;
	@XmlElements({
        @XmlElement(type=OtherContent.class),
        @XmlElement(type=RelazioneScavo.class),
        @XmlElement(type=SDILayerDescriptor.class),
        @XmlElement(type=LayerConcessione.class),
        @XmlElement(type=UploadedImage.class),
    })
	private AssociatedContent toRegister;
	private List<SHUBFileDescriptor> streams=new ArrayList<AddSectionToConcessioneRequest.SHUBFileDescriptor>();

}
