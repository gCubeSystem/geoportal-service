package org.gcube.application.geoportal.service;

import javax.ws.rs.ApplicationPath;

import org.gcube.application.geoportal.common.rest.InterfaceConstants;
import org.gcube.application.geoportal.service.rest.Concessioni;
import org.gcube.application.geoportal.service.rest.ConcessioniOverMongo;
import org.gcube.application.geoportal.service.rest.Profiles;
import org.gcube.application.geoportal.service.rest.Projects;
import org.gcube.application.geoportal.service.rest.Sections;
import org.glassfish.jersey.server.ResourceConfig;

@ApplicationPath(InterfaceConstants.APPLICATION_PATH)
public class GeoPortalService extends ResourceConfig{

	
	
	public GeoPortalService() {
		super();
		//Register interrfaces
		registerClasses(Concessioni.class);
		registerClasses(ConcessioniOverMongo.class);
		registerClasses(Projects.class);
		registerClasses(Sections.class);
		registerClasses(Profiles.class);
		
		
	}
	
	
}
