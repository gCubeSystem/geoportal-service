package org.gcube.application.geoportal.service.engine.cache;

import org.gcube.application.geoportal.model.fault.ConfigurationException;
import org.gcube.application.geoportal.service.engine.AbstractScopedMap;
import org.gcube.application.geoportal.service.engine.ImplementationProvider;
import org.gcube.application.geoportal.service.model.internal.db.MongoConnection;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;

import lombok.extern.slf4j.Slf4j;
@Slf4j
public class MongoClientProvider extends AbstractScopedMap<MongoClient>{

	public MongoClientProvider() {
		super("MongoClient cache");
		
//		setTTL(Duration.of(10,ChronoUnit.MINUTES));
	}

	@Override
	protected MongoClient retrieveObject() throws ConfigurationException {
		MongoConnection conn=ImplementationProvider.get().getMongoConnectionProvider().getObject();
		log.debug("Connecting to "+conn);
		
		MongoCredential credential = MongoCredential.createCredential(conn.getUser(), conn.getDatabase(), 
				conn.getPassword().toCharArray());

		
		
		 MongoClientOptions options = MongoClientOptions.builder().sslEnabled(true).build();

		 return new MongoClient(new ServerAddress(conn.getHosts().get(0),conn.getPort()),
		                                           credential,
		                                           options);
	}
	
	@Override
	protected void dispose(MongoClient toDispose) {
		toDispose.close();
	}
	
	@Override
	public void init() {
		// TODO Auto-generated method stub
	}
}
