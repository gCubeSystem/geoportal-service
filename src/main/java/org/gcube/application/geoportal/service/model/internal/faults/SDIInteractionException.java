package org.gcube.application.geoportal.service.model.internal.faults;

public class SDIInteractionException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public SDIInteractionException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public SDIInteractionException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public SDIInteractionException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public SDIInteractionException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public SDIInteractionException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	
	
}
