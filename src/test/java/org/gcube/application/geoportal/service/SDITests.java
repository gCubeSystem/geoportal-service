package org.gcube.application.geoportal.service;

import org.gcube.application.geoportal.model.fault.ConfigurationException;
import org.gcube.application.geoportal.service.engine.SDIManager;
import org.gcube.application.geoportal.service.engine.mongo.PostgisIndex;
import org.gcube.application.geoportal.service.legacy.TokenSetter;
import org.gcube.application.geoportal.service.model.internal.faults.SDIInteractionException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.validation.constraints.AssertTrue;
import java.sql.SQLException;
import java.util.regex.Matcher;

public class SDITests {


    @Before
    public void init(){
        TokenSetter.set("/gcube/devsec/devVRE");
    }

    @Test
    public void registerCentroidsLayer() throws SDIInteractionException, SQLException, ConfigurationException {
        PostgisIndex index=new PostgisIndex();
    }

    @Test
    public void testRegexp(){
        Matcher hostMatcher=SDIManager.HOSTNAME_PATTERN.matcher("jdbc:postgresql://postgresql-srv-dev.d4science.org:5432/geoserver_dev_db");
        Assert.assertTrue(hostMatcher.find());
        System.out.println("HOST :\t"+hostMatcher.group());

        Matcher portMatcher=SDIManager.PORT_PATTERN.matcher("jdbc:postgresql://postgresql-srv-dev.d4science.org:5432/geoserver_dev_db");
        Assert.assertTrue(portMatcher.find());
        System.out.println("PORT :\t"+portMatcher.group());

        Matcher dbMatcher=SDIManager.DB_NAME_PATTERN.matcher("jdbc:postgresql://postgresql-srv-dev.d4science.org:5432/geoserver_dev_db");
        Assert.assertTrue(dbMatcher.find());
        System.out.println("DB :\t"+dbMatcher.group());
    }
}
