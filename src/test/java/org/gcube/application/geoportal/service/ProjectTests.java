package org.gcube.application.geoportal.service;

import java.util.Collections;
import java.util.List;

import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

import org.bson.Document;
import org.gcube.application.geoportal.common.model.project.Project;
import org.gcube.application.geoportal.common.rest.InterfaceConstants;
import org.junit.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

public class ProjectTests extends BasicServiceTestUnit{
	
	String testProfileId="profiled_concessione";
	String projectId="asdlkjgdasfjklgadjhkl";
	
	@Test
	public void getAll() {
		WebTarget target=target(InterfaceConstants.Methods.PROJECTS);
		System.out.println(target.request(MediaType.APPLICATION_JSON).get(List.class));
	}
	
	@Test
	public void getFilteredAll() {
		WebTarget target=target(InterfaceConstants.Methods.PROJECTS);
		Document document =new Document(Collections.singletonMap("key", "value"));
		
		System.out.println(target.path("search").request(MediaType.APPLICATION_JSON).
				post(Entity.entity(document, MediaType.APPLICATION_JSON)));
		
	}
	
	@Test 
	public void getAllByProfile() {
		WebTarget target=target(InterfaceConstants.Methods.PROJECTS);
		System.out.println(target.path(testProfileId).request(MediaType.APPLICATION_JSON).get(List.class));
	}
	
	@Test
	public void getFilteredByProfile() {
		WebTarget target=target(InterfaceConstants.Methods.PROJECTS);
		Document document =new Document(Collections.singletonMap("key", "value"));
		
		System.out.println(target.path("search").path(testProfileId).request(MediaType.APPLICATION_JSON).
				post(Entity.entity(document, MediaType.APPLICATION_JSON)));
		
	}
	
	
	@Test
	public void getById() {
		WebTarget target=target(InterfaceConstants.Methods.PROJECTS);
		System.out.println(target.path(testProfileId).path(projectId).request(MediaType.APPLICATION_JSON).get().readEntity(String.class));
	}
	
	
	@Test 
	public void registerNew() {
		WebTarget target=target(InterfaceConstants.Methods.PROJECTS);
		Document document =new Document(Collections.singletonMap("key", "value"));
		
		System.out.println(target.path(testProfileId).request(MediaType.APPLICATION_JSON).
				put(Entity.entity(document, MediaType.APPLICATION_JSON)));
	}
	
	@Test 
	public void updateDocument() {
		WebTarget target=target(InterfaceConstants.Methods.PROJECTS);
		Document document =new Document(Collections.singletonMap("key", "value"));
		
		System.out.println(target.path(testProfileId).path(projectId).request(MediaType.APPLICATION_JSON).
				put(Entity.entity(document, MediaType.APPLICATION_JSON)));
	}
}
