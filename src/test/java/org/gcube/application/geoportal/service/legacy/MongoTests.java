package org.gcube.application.geoportal.service.legacy;

import java.io.IOException;

import org.bson.Document;
import org.gcube.application.geoportal.common.model.profile.Profile;
import org.gcube.application.geoportal.common.utils.Files;
import org.gcube.application.geoportal.model.fault.ConfigurationException;
import org.gcube.application.geoportal.service.engine.ImplementationProvider;
import org.gcube.application.geoportal.service.engine.cache.MongoClientProvider;
import org.gcube.application.geoportal.service.engine.mongo.MongoManager;
import org.gcube.application.geoportal.service.utils.Serialization;
import org.junit.BeforeClass;
import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.mongodb.Block;
import com.mongodb.MongoClient;

public class MongoTests {

	@BeforeClass
	public static final void init() {
		ImplementationProvider.get().setMongoClientProvider(new MongoClientProvider() {
			@Override
			public MongoClient getObject() throws ConfigurationException {
				TokenSetter.set("/gcube/devNext/NextNext");
				return super.getObject();
			}
		});
	}

	
	Block<Document> printBlock = new Block<Document>() {
	       @Override
	       public void apply(final Document document) {
	           System.out.println(document.toJson());
	       }
	};
	
	@Test
	public void listProfiles() throws JsonProcessingException, IOException, ConfigurationException {
//		MongoManager manager=new MongoManager();
//		Profile f=Serialization.mapper.readerFor(Profile.class).readValue(
//				Files.getFileFromResources("fakeProfile.json"));
//		
//		manager.iterate(new Document(),f).forEach(printBlock);
	}
	
//	@Test
//	public void writeProject() {
//		MongoManager manager=new MongoManager();
//		Concessione f=Serialization.mapper.readerFor(Concessione.class).readValue(
//				Files.getFileFromResources("fakeProfile.json"));
//	}
}
