package org.gcube.application.geoportal.service.rest;

import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

import org.gcube.application.geoportal.common.rest.InterfaceConstants;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Path(InterfaceConstants.Methods.SECTIONS+"/{"+InterfaceConstants.Parameters.PROJECT_ID+"}")
public class Sections {

	@PathParam(InterfaceConstants.Parameters.PROJECT_ID) String projectID;
	
	
	// Add Section to Project Document
//		@PUT
//		@Produces(MediaType.APPLICATION_JSON)
//		@Consumes(MediaType.APPLICATION_JSON)
//		public Section registerNewSection(Section toRegister) {
//			try {
//				log.info("Creating new Section [PROJECT_ID:{}]",projectID);
//				throw new RuntimeException("Feature not yet available");
//			}catch(WebApplicationException e){
//				log.warn("Unable to serve request",e);
//				throw e;
//			}catch(Throwable e){
//				log.warn("Unable to serve request",e);
//				throw new WebApplicationException("Unable to serve request", e);
//			}
//		}
//	
//		
//		// Remove Section to Project Document
//		@DELETE
//		@Produces(MediaType.APPLICATION_JSON)
//		@Consumes(MediaType.APPLICATION_JSON)
//		@Path("{"+InterfaceConstants.Parameters.SECTION_ID+"}")
//		public Project deleteSection(@QueryParam(InterfaceConstants.Parameters.SECTION_ID) String sectionID) {
//			try {
//				log.info("Deleting Section [ID : {}, PROJECT_ID:{}]",projectID,sectionID);
//				throw new RuntimeException("Feature not yet available");
//			}catch(WebApplicationException e){
//				log.warn("Unable to serve request",e);
//				throw e;
//			}catch(Throwable e){
//				log.warn("Unable to serve request",e);
//				throw new WebApplicationException("Unable to serve request", e);
//			}
//		}
}
