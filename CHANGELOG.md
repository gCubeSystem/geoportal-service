This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for org.gcube.application.geoportal-service

## [v1.0.5-SNAPSHOT] 2021-07-23
Upgrade to gcube-smartgears-bom 2.1.0
Fix register postgis table layer
Added PostgisIndexRecordManager

## [v1.0.4] 2020-11-11
Mongo integration with Concessione
Project interface
TempFile management
WorkspaceContent and publication for Concessioni-over-mongo 

## [v1.0.3] 2020-11-11
Fixed HTTP method

## [v1.0.2] 2020-11-11
Delete method
Excluded upper bound release gCube 5


## [v1.0.1] 2020-11-11

Project interface

## [v1.0.0] 2020-11-11

First release


