package org.gcube.application.geoportal.service.engine;

import org.gcube.application.geoportal.model.fault.ConfigurationException;
import org.gcube.common.storagehub.client.dsl.StorageHubClient;

public class StorageHubProvider implements Engine<StorageHubClient>{

	
	@Override
	public StorageHubClient getObject() throws ConfigurationException {
		return new StorageHubClient();
	}
	
	@Override
	public void init() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void shustdown() {
		// TODO Auto-generated method stub
		
	}
	
}
