package org.gcube.application.geoportal.service.engine;

import java.time.LocalDateTime;
import java.time.temporal.TemporalAmount;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.BiFunction;

import org.gcube.application.geoportal.model.fault.ConfigurationException;
import org.gcube.application.geoportal.service.utils.ContextUtils;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.Synchronized;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RequiredArgsConstructor
public abstract class AbstractScopedMap<T> implements Engine<T>{

	// scope-> object
	private ConcurrentHashMap<String,TTLObject<T>> scopeMap=new ConcurrentHashMap<String,TTLObject<T>>();
	
	@Setter
	private TemporalAmount TTL=null;
	
	@NonNull
	private String name;
	@Synchronized
	public T getObject() throws ConfigurationException {
		String currentScope=ContextUtils.getCurrentScope();
		log.debug(name+" : obtaining object for context "+currentScope);
		scopeMap.putIfAbsent(currentScope, new TTLObject<T>(LocalDateTime.now(),retrieveObject()));
		TTLObject<T> found=scopeMap.get(currentScope);
		
		if(TTL!=null) {
			if(!found.getCreationTime().plus(TTL).isBefore(LocalDateTime.now())) {
				log.debug(name+" : elapsed TTL, disposing..");
				dispose(found.getTheObject());
				found=scopeMap.put(currentScope, new TTLObject<T>(LocalDateTime.now(),retrieveObject()));
			}
		}
		return found.getTheObject();
	}
	
	
	@Override
	public void shustdown() {
		log.warn(name + ": shutting down");
		scopeMap.forEach((String s,TTLObject<T> o)->{
			try{if(o!=null&&o.getTheObject()!=null)
				dispose(o.getTheObject());
			}catch(Throwable t) {
				log.warn(name +": unable to dispose ",t); 
			}
		});
	}
	
	protected abstract T retrieveObject() throws ConfigurationException; 
	
	protected abstract void dispose(T toDispose);
}
