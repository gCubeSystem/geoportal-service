package org.gcube.application.geoportal.service.rest;

import java.time.LocalDateTime;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;

import org.gcube.application.geoportal.common.model.legacy.Concessione;
import org.gcube.application.geoportal.common.rest.AddSectionToConcessioneRequest;
import org.gcube.application.geoportal.common.rest.InterfaceConstants;
import org.gcube.application.geoportal.common.rest.TempFile;
import org.gcube.application.geoportal.service.engine.WorkspaceManager;
import org.gcube.application.geoportal.service.engine.WorkspaceManager.FolderOptions;
import org.gcube.application.geoportal.service.engine.mongo.ConcessioniMongoManager;
import org.gcube.application.geoportal.service.utils.Serialization;
import org.gcube.common.storagehub.client.dsl.FolderContainer;
import org.json.JSONArray;
import org.json.JSONObject;

import lombok.extern.slf4j.Slf4j;

@Path(InterfaceConstants.Methods.MONGO_CONCESSIONI)
@Slf4j
public class ConcessioniOverMongo {

	
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String replace(String jsonString) {
		return new GuardedMethod<String> () {
			@Override
			protected String run() throws Exception, WebApplicationException {
				Concessione c=Serialization.read(jsonString, Concessione.class);
				ConcessioniMongoManager manager=new ConcessioniMongoManager();
				manager.replace(c);
				
				return Serialization.write(manager.getById(c.getMongo_id()));
			}
		}.execute().getResult();
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String createNew(String jsonString) {
		return new GuardedMethod<String> () {
			@Override
			protected String run() throws Exception, WebApplicationException {
				Concessione c=Serialization.read(jsonString, Concessione.class);
				ConcessioniMongoManager manager=new ConcessioniMongoManager();
				return Serialization.write(manager.registerNew(c));				
			}
		}.execute().getResult();
	}
	
	
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String list() {
		return new GuardedMethod<String> () {
			protected String run() throws Exception ,WebApplicationException {
				ConcessioniMongoManager manager=new ConcessioniMongoManager();
				JSONArray toReturn=new JSONArray();
				manager.list().forEach((Concessione c) -> {
					try{
					toReturn.put(new JSONObject(Serialization.write(c)));
					}catch(Throwable t) {
						log.error("Unable to serialize "+c);
					}
				});
			return toReturn.toString();
			
		};
	}.execute().getResult();
				
		
	}
	
	
	// BY ID 
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("{"+InterfaceConstants.Parameters.PROJECT_ID+"}")
	public String getById(@PathParam(InterfaceConstants.Parameters.PROJECT_ID) String id) {
		return new GuardedMethod<String> () {
			@Override
			protected String run() throws Exception, WebApplicationException {
				ConcessioniMongoManager manager=new ConcessioniMongoManager();				
				return Serialization.write(manager.getById(id));
			}
		}.execute().getResult();
	}
	
	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	@Path("{"+InterfaceConstants.Parameters.PROJECT_ID+"}")
	public void deleteById(@PathParam(InterfaceConstants.Parameters.PROJECT_ID) String id) {
		new GuardedMethod<Concessione> () {
			@Override
			protected Concessione run() throws Exception, WebApplicationException {
				ConcessioniMongoManager manager=new ConcessioniMongoManager();				
				manager.deleteById(id);
				return null;
			}
		}.execute();
	}
	
	
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("{"+InterfaceConstants.Parameters.PROJECT_ID+"}")
	public String update(@PathParam(InterfaceConstants.Parameters.PROJECT_ID) String id,String jsonString) {
		return new GuardedMethod<String> () {
			@Override
			protected String run() throws Exception, WebApplicationException {
//				Concessione c=Serialization.read(jsonString, Concessione.class);
//				ConcessioniMongoManager manager=new ConcessioniMongoManager();
//				manager.update(c);
//				
//				return Serialization.write(manager.getById(c.getMongo_id()));
				throw new RuntimeException("TO IMPLEMENT");
			}
		}.execute().getResult();
	}
	
	
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{"+InterfaceConstants.Methods.PUBLISH_PATH+"}/{"+InterfaceConstants.Parameters.PROJECT_ID+"}")
	public String publish(@PathParam(InterfaceConstants.Parameters.PROJECT_ID) String id) {
		return new GuardedMethod<String> () {
			@Override
			protected String run() throws Exception, WebApplicationException {
				ConcessioniMongoManager manager=new ConcessioniMongoManager();				
				return Serialization.write(manager.publish(id));				
			}
		}.execute().getResult();
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/"+InterfaceConstants.Methods.REGISTER_FILES_PATH+"/{"+InterfaceConstants.Parameters.PROJECT_ID+"}")
	public String registerFile(@PathParam(InterfaceConstants.Parameters.PROJECT_ID) String id,String jsonRequest) {
		return new GuardedMethod<String> () {
			@Override
			protected String run() throws Exception, WebApplicationException {
				AddSectionToConcessioneRequest request=Serialization.read(jsonRequest,AddSectionToConcessioneRequest.class);
				log.info("Registering {} file(s) for {} Concessione ID {}",
						request.getStreams().size(),
						request.getDestinationPath(),id);
				ConcessioniMongoManager manager=new ConcessioniMongoManager();
				Concessione toReturn= manager.persistContent(id, request.getDestinationPath(), request.getStreams());
				
				log.debug("Returning "+toReturn);
				return Serialization.write(toReturn);				
			}
		}.execute().getResult();
	}
	
}
