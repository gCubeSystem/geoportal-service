package org.gcube.application.geoportal.service.engine.cache;

import java.time.Duration;
import java.time.temporal.ChronoUnit;

import org.gcube.application.geoportal.model.fault.ConfigurationException;
import org.gcube.application.geoportal.service.ServiceConstants;
import org.gcube.application.geoportal.service.engine.AbstractScopedMap;
import org.gcube.application.geoportal.service.model.internal.db.MongoConnection;
import org.gcube.application.geoportal.service.utils.ISUtils;

public class MongoConnectionProvider extends AbstractScopedMap<MongoConnection>{

	public MongoConnectionProvider() {
		super("MongoDBInfo Cache");
		setTTL(Duration.of(2,ChronoUnit.MINUTES));
	}
	
	@Override
	protected MongoConnection retrieveObject() throws ConfigurationException {		
		return ISUtils.queryForMongoDB(ServiceConstants.MONGO_SE_PLATFORM, ServiceConstants.MONGO_SE_GNA_FLAG);
	}
	
	@Override
	protected void dispose(MongoConnection toDispose) {
		
	}
	
	@Override
	public void init() {
		// TODO Auto-generated method stub
	}
}
