package org.gcube.application.geoportal.service.engine.mongo;


import static com.mongodb.client.model.Filters.eq;

import org.bson.Document;
import org.bson.types.ObjectId;
import org.gcube.application.geoportal.model.fault.ConfigurationException;
import org.gcube.application.geoportal.service.engine.ImplementationProvider;

import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.FindOneAndReplaceOptions;
import com.mongodb.client.model.FindOneAndUpdateOptions;
import com.mongodb.client.model.ReturnDocument;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public abstract class MongoManager {

	protected MongoClient client=null;
	
	protected static final String ID="_id";
	
	protected static final ObjectId asId(String id) {return new ObjectId(id);}
	protected static final String asString(ObjectId id) {return id.toHexString();}
	
	protected static final String asString(Document d) {return d.toJson();}
	protected static final Document asDoc(String json) {return Document.parse(json);}
	
	public MongoManager() throws ConfigurationException {
		client=ImplementationProvider.get().getMongoClientProvider().getObject();
		
		log.info("Got Mongo Client at "+client.getConnectPoint());
		// NOT AUTHORIZED
//		log.debug("Existing databases "+client.getDatabaseNames());		
	}
	
//	private abstract MongoDatabase getDatabase() {
//		return client.getDatabase("gna-db");
//	}
	
	// TODO check if existing DB
	protected abstract MongoDatabase getDatabase();
	
	//*********** PROJECTS
	// NB BsonId
	protected ObjectId insert(Document proj, String collectionName) {		
		MongoDatabase database=getDatabase();				
		MongoCollection<Document> collection = database.getCollection(collectionName);
		
		// Check if _id is present
		ObjectId id=proj.getObjectId(ID);
		if(id==null) {
			proj.append(ID, new ObjectId());
			id=proj.getObjectId(ID);
		}
		
		
		collection.insertOne(Document.parse(proj.toJson()));	
		return id;		
	}
	
	public void delete(ObjectId id, String collectionName) {
		MongoDatabase database=getDatabase();		
		MongoCollection<Document> collection = database.getCollection(collectionName);		
		collection.deleteOne(eq(ID,id));
	}
	
	
	
	public Document getById(ObjectId id,String collectionName) {
		MongoDatabase database=getDatabase();
		MongoCollection<Document> coll=database.getCollection(collectionName);		
		return coll.find(new Document(ID,id)).first();
	}
	
	
	public FindIterable<Document> iterate(Document filter,String collectionName) {
		MongoDatabase database=getDatabase();
		MongoCollection<Document> coll=database.getCollection(collectionName);		
		if(filter==null)
			return coll.find();
		else
			return coll.find(filter);
	}
	
	
	public <T> FindIterable<T> iterateForClass(Document filter,String collectionName,Class<T> clazz) {
		MongoDatabase database=getDatabase();
		MongoCollection<Document> coll=database.getCollection(collectionName);
		if(filter==null)
			return coll.find(clazz);
		else
			return coll.find(filter,clazz);				
	}
	
	public Document replace(Document toUpdate,String collectionName) {
		MongoDatabase database=getDatabase();
		MongoCollection<Document> coll=database.getCollection(collectionName);
		return coll.findOneAndReplace(
				eq(ID,toUpdate.getObjectId(ID)), toUpdate,new FindOneAndReplaceOptions().returnDocument(ReturnDocument.AFTER));
		
	}
	
	public Document update(ObjectId id, Document updateSet, String collectionName) {
		MongoDatabase database=getDatabase();
		MongoCollection<Document> coll=database.getCollection(collectionName);
		return coll.findOneAndUpdate(
				eq(ID,id), 
				updateSet,
				new FindOneAndUpdateOptions().returnDocument(ReturnDocument.AFTER));
	}
	
	//********** PROFILES
	
	
}
