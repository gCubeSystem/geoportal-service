package org.gcube.application.geoportal.service;

import javax.persistence.EntityManagerFactory;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.Response;

import org.gcube.application.geoportal.managers.AbstractRecordManager;
import org.gcube.application.geoportal.model.fault.ConfigurationException;
import org.gcube.application.geoportal.model.report.PublicationReport;
import org.gcube.application.geoportal.service.engine.ImplementationProvider;
import org.gcube.application.geoportal.service.engine.ScopedEMFProvider;
import org.gcube.application.geoportal.service.engine.StorageClientProvider;
import org.gcube.application.geoportal.service.engine.cache.MongoClientProvider;
import org.gcube.application.geoportal.service.engine.cache.MongoConnectionProvider;
import org.gcube.application.geoportal.service.legacy.TokenSetter;
import org.gcube.application.geoportal.service.utils.Serialization;
import org.gcube.contentmanagement.blobstorage.service.IClient;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.BeforeClass;

import com.mongodb.MongoClient;

public class BasicServiceTestUnit extends JerseyTest {

	
	
	@Override
	protected Application configure() {
		return new GeoPortalService();
	}
	

	
	@BeforeClass
	public static void init() {
		String scope="/gcube/devsec/devVRE";
		AbstractRecordManager.setDefaultProvider(
		
		new ScopedEMFProvider(){
			
			@Override
			public EntityManagerFactory getFactory() {
				TokenSetter.set(scope);
				return super.getFactory();
			}
		});
		
		ImplementationProvider.get().setStorageProvider(new StorageClientProvider() {
			@Override
			public IClient getObject() throws ConfigurationException {
				TokenSetter.set(scope);
				return super.getObject();
			}
		});
		
		
		ImplementationProvider.get().setMongoConnectionProvider(new MongoConnectionProvider() {
			@Override
			public org.gcube.application.geoportal.service.model.internal.db.MongoConnection getObject() throws ConfigurationException {
				TokenSetter.set(scope);
				return super.getObject();
			}
		});
		
		ImplementationProvider.get().setMongoClientProvider(new MongoClientProvider() {
			@Override
			public MongoClient getObject() throws ConfigurationException {
				TokenSetter.set(scope);
				return super.getObject();
			}
		});
		
	}
	
	
	protected static<T> T check(Response resp, Class<T> clazz) throws Exception {
		String resString=resp.readEntity(String.class);
		if(resp.getStatus()<200||resp.getStatus()>=300)
			throw new Exception("RESP STATUS IS "+resp.getStatus()+". Message : "+resString);
		System.out.println("Resp String is "+resString);
		if(clazz!=null)
			return Serialization.read(resString, clazz);
		else return null;
	}
}
