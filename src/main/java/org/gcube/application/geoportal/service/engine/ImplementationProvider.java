package org.gcube.application.geoportal.service.engine;

import org.gcube.application.geoportal.managers.AbstractRecordManager;
import org.gcube.application.geoportal.managers.EMFProvider;
import org.gcube.application.geoportal.service.engine.cache.MongoClientProvider;
import org.gcube.application.geoportal.service.engine.cache.MongoConnectionProvider;

import lombok.Getter;
import lombok.Setter;
import lombok.Synchronized;

public class ImplementationProvider {

	private static ImplementationProvider instance=null;
	
	@Synchronized
	public static ImplementationProvider get() {
		if(instance==null) {
			instance=new ImplementationProvider();
		}
		return instance;
	}
	
	
	@Getter
	@Setter
	private MongoConnectionProvider mongoConnectionProvider=new MongoConnectionProvider();
	
	@Getter
	@Setter
	private MongoClientProvider mongoClientProvider=new MongoClientProvider();
	
	
	@Getter
	@Setter
	private StorageClientProvider storageProvider=new StorageClientProvider();
	
	
	@Getter
	@Setter
	private EMFProvider emfProvider=new ScopedEMFProvider();
	
	
	@Getter
	@Setter
	private StorageHubProvider sHubProvider=new StorageHubProvider();
	
	public void shutdown() {
		// Stop JPA
		AbstractRecordManager.shutdown();
		mongoConnectionProvider.shustdown();
		mongoClientProvider.shustdown();
	}
	
	public void startup() {
		AbstractRecordManager.setDefaultProvider(emfProvider);
		mongoConnectionProvider.init();
		mongoClientProvider.init();
	}
}
