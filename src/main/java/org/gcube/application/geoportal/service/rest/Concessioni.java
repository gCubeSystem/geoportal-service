	package org.gcube.application.geoportal.service.rest;

import java.util.Collection;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response.Status;

import org.gcube.application.geoportal.common.rest.InterfaceConstants;
import org.gcube.application.geoportal.managers.ConcessioneManager;
import org.gcube.application.geoportal.managers.ManagerFactory;
import org.gcube.application.geoportal.model.InputStreamDescriptor;
import org.gcube.application.geoportal.model.concessioni.Concessione;
import org.gcube.application.geoportal.model.concessioni.LayerConcessione;
import org.gcube.application.geoportal.model.concessioni.RelazioneScavo;
import org.gcube.application.geoportal.model.content.UploadedImage;
import org.gcube.application.geoportal.model.report.PublicationReport;
import org.gcube.application.geoportal.service.engine.ImplementationProvider;
import org.gcube.application.geoportal.service.engine.StorageClientProvider;
import org.gcube.application.geoportal.service.model.internal.rest.AddSectionToConcessioneRequest;
import org.gcube.application.geoportal.service.model.internal.rest.AddSectionToConcessioneRequest.SHUBFileDescriptor;
import org.gcube.application.geoportal.service.utils.Serialization;
import org.json.JSONArray;

import lombok.extern.slf4j.Slf4j;

@Path(InterfaceConstants.Methods.CONCESSIONI)
@Slf4j
public class Concessioni {

	
	
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("publish/{"+InterfaceConstants.Parameters.PROJECT_ID+"}")
	public String publish(@PathParam(InterfaceConstants.Parameters.PROJECT_ID) String id) {
		try {
			log.info("Publishing Concessione by id {} ",id);
			Concessione conc=(Concessione)  ConcessioneManager.getByID(Long.parseLong(id));
			ConcessioneManager manager=ManagerFactory.getByRecord(conc);
			log.debug("Loaded object {} ",conc);
			
			PublicationReport rep=manager.commitSafely(true);
			String toReturn=rep.prettyPrint();
			log.debug("Publication report to send is "+toReturn);
			return toReturn;
		}catch(WebApplicationException e){
			log.warn("Unable to serve request",e);
			throw e;
		}catch(Throwable e){
			log.warn("Unable to serve request",e);
			throw new WebApplicationException("Unable to serve request", e);
		}
	}
	
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("{"+InterfaceConstants.Parameters.PROJECT_ID+"}")
	public String getById(@PathParam(InterfaceConstants.Parameters.PROJECT_ID) String id) {
		try {
			log.info("Loading Concessione by id {} ",id);
			Concessione toReturn=(Concessione)  ConcessioneManager.getByID(Long.parseLong(id));
			if(toReturn==null)
				throw new WebApplicationException("Concessione non trovata",Status.NOT_FOUND);
			log.debug("Loaded object {} ",toReturn);
			return Serialization.write(toReturn);
		}catch(WebApplicationException e){
			log.warn("Unable to serve request",e);
			throw e;
		}catch(Throwable e){
			log.warn("Unable to serve request",e);
			throw new WebApplicationException("Unable to serve request", e);
		}
	}

	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String registerNew(String toRegister) {
		try {
			log.info("Registering new Concessione "+toRegister);
			Concessione conc=Serialization.read(toRegister, Concessione.class);
			ConcessioneManager manager=ManagerFactory.registerNew(conc);
			manager.commitSafely(false);			
			return Serialization.write(manager.getRecord());
		}catch(WebApplicationException e){
			log.warn("Unable to serve request",e);
			throw e;
		}catch(Throwable e){
			log.warn("Unable to serve request",e);
			throw new WebApplicationException("Unable to serve request", e);
		}
	}

	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("section/{"+InterfaceConstants.Parameters.PROJECT_ID+"}")
	public String addSection(@PathParam(InterfaceConstants.Parameters.PROJECT_ID) String id,
			AddSectionToConcessioneRequest request) {
		try {
			log.info("Adding section to Concessione {} ",id);
			Concessione toReturn=(Concessione)  ConcessioneManager.getByID(Long.parseLong(id));
			ConcessioneManager manager=ManagerFactory.getByRecord(toReturn);
			log.debug("Loaded object {} ",toReturn);
			log.debug("Request is {}",request);

			InputStreamDescriptor[] streams=new InputStreamDescriptor[request.getStreams().size()];
			
			StorageClientProvider storage=ImplementationProvider.get().getStorageProvider();
			
			for(int i=0;i<streams.length;i++) {
				SHUBFileDescriptor sent=request.getStreams().get(i);
				streams[i]=new InputStreamDescriptor(storage.open(sent.getShubID()), sent.getFilename());
			}
			
			
			switch(request.getSection()) {
			case PIANTA :manager.addPiantaFineScavo((LayerConcessione) request.getToRegister(),streams);
			break;
			case POSIZIONAMENTO : manager.setPosizionamento((LayerConcessione) request.getToRegister(),streams);
			break;
			case RELAZIONE : manager.setRelazioneScavo((RelazioneScavo)request.getToRegister(), streams[0]);
			break;
			case UPLOADED_IMG : manager.addImmagineRappresentativa((UploadedImage)request.getToRegister(), streams[0]);
			break;
			default : throw new Exception("Unrecognized section");
			}
			
//			PublicationReport report=manager.commitSafely(false);
			Concessione c=manager.commit(false);
			log.debug("Published "+Serialization.write(c));
			return c.validate().prettyPrint(); 
		}catch(WebApplicationException e){
			log.warn("Unable to serve request",e);
			throw e;
		}catch(Throwable e){
			log.warn("Unable to serve request",e);
			throw new WebApplicationException("Unable to serve request", e);
		}
	}

	
	
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String getList(){
		try {
			Collection<Concessione> toReturn=ManagerFactory.getList(Concessione.class);
			log.debug("Found "+toReturn.size()+" elements..");
			JSONArray array=new JSONArray();
			for(Concessione found:toReturn) {
				array.put(Serialization.write(found));
			}
			return array.toString();
		}catch(WebApplicationException e){
			log.warn("Unable to serve request",e);
			throw e;
		}catch(Throwable e){
			log.warn("Unable to serve request",e);
			throw new WebApplicationException("Unable to serve request", e);
		}
	}

}
