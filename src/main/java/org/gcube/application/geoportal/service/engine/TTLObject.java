package org.gcube.application.geoportal.service.engine;

import java.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@AllArgsConstructor
public class TTLObject<T> {

	private LocalDateTime creationTime;
	private T theObject;
	
}
