package org.gcube.application.geoportal.service.engine;

import java.io.FileNotFoundException;
import java.io.InputStream;

import javax.validation.constraints.NotNull;

import org.gcube.application.geoportal.common.model.legacy.WorkspaceContent;
import org.gcube.application.geoportal.common.utils.Files;
import org.gcube.application.geoportal.model.fault.ConfigurationException;
import org.gcube.common.storagehub.client.dsl.FileContainer;
import org.gcube.common.storagehub.client.dsl.FolderContainer;
import org.gcube.common.storagehub.client.dsl.StorageHubClient;
import org.gcube.common.storagehub.model.exceptions.StorageHubException;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.Synchronized;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class WorkspaceManager {

	private static final String APP_FOLDER=".GNA_RECORDS";


	private StorageHubClient sgClient=null;
	private FolderContainer appBase=null;

	@Getter
	@Setter
	@AllArgsConstructor
	@RequiredArgsConstructor
	public static class FolderOptions{
		@NotNull
		private String folderName;
		private String folderDescription;
		private FolderContainer parent;
	}
	
	@Getter
	@Setter
	@AllArgsConstructor
	@RequiredArgsConstructor
	public static class FileOptions{
		@NotNull
		private String fileName;
		@NonNull
		private InputStream is;
		
		private String fileDescription;
		private FolderContainer parent;
		
	}


	public WorkspaceManager() throws ConfigurationException, StorageHubException {
		sgClient= ImplementationProvider.get().getSHubProvider().getObject();
		appBase=getApplicationBaseFolder(sgClient);
	}

	public FolderContainer createFolder(FolderOptions opts) throws StorageHubException {
		if(opts.getParent()==null)
			opts.setParent(appBase);
		return createFolder(opts,sgClient);		
	}

	public FileContainer getFileById(String id) throws StorageHubException {
		return sgClient.open(id).asFile();
	}

	public FolderContainer getFolderById(String id) throws StorageHubException {
		return sgClient.open(id).asFolder();
	}

	public FolderContainer getSubFolder(FolderContainer parentFolder,String path) throws StorageHubException {		
		try{
			return parentFolder.openByRelativePath(path).asFolder();
		}catch(StorageHubException e) {			
			log.debug("Missing subPath "+path);
			FolderContainer targetParent=parentFolder;
			String targetName=path;
			if(path.contains("/")) {
				String parent=path.substring(0, path.lastIndexOf("/"));
				log.debug("Checking intermediate "+parent);
				targetParent=getSubFolder(parentFolder,parent);
				targetName=path.substring(path.lastIndexOf("/")+1);
			}
			log.debug("Creating "+targetName);
			return createFolder(new FolderOptions(targetName,"",targetParent),sgClient);
		}
	}


	public WorkspaceContent storeToWS(FileOptions opts) throws FileNotFoundException, StorageHubException {
		FileContainer item=createFile(opts,sgClient);
		item=sgClient.open(item.getId()).asFile();

		WorkspaceContent content=new WorkspaceContent();
		content.setLink(item.getPublicLink().toString());
		content.setMimetype(item.get().getContent().getMimeType());
		content.setStorageID(item.getId());
		return content;	

	}

	public void deleteFromWS(WorkspaceContent toDelete) throws StorageHubException {
		sgClient.open(toDelete.getStorageID()).asFile().forceDelete();		
	}
	
	// STATIC SYNCH METHODS 
	
	@Synchronized
	private static FolderContainer getApplicationBaseFolder(StorageHubClient sgClient) throws StorageHubException {
		FolderContainer vre=sgClient.openVREFolder();
		try {
			return vre.openByRelativePath(APP_FOLDER).asFolder();
		}catch(StorageHubException e) {
			log.debug("APP Fodler missing. Initializing..");
			FolderContainer toReturn= vre.newFolder(APP_FOLDER, "Base folder for GNA records");
			toReturn.setHidden();
			return toReturn;
		}
	}

	@Synchronized
	private static FolderContainer createFolder(FolderOptions opts, StorageHubClient sgClient) throws StorageHubException {
		opts.setFolderName(Files.fixFilename(opts.getFolderName()));
		return opts.getParent().newFolder(opts.getFolderName(),opts.getFolderDescription());
	}
	
	@Synchronized
	private static FileContainer createFile(FileOptions opts, StorageHubClient sgClient) throws StorageHubException {
		opts.setFileName(Files.fixFilename(opts.getFileName()));
		return opts.getParent().uploadFile(opts.getIs(), opts.getFileName(), opts.getFileDescription());
	}
}
