package org.gcube.application.geoportal.service.engine.mongo;

import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import org.bson.Document;
import org.bson.types.ObjectId;
import org.gcube.application.geoportal.common.model.legacy.AssociatedContent;
import org.gcube.application.geoportal.common.model.legacy.Concessione;
import org.gcube.application.geoportal.common.model.legacy.GeoServerContent;
import org.gcube.application.geoportal.common.model.legacy.LayerConcessione;
import org.gcube.application.geoportal.common.model.legacy.OtherContent;
import org.gcube.application.geoportal.common.model.legacy.PersistedContent;
import org.gcube.application.geoportal.common.model.legacy.RelazioneScavo;
import org.gcube.application.geoportal.common.model.legacy.SDILayerDescriptor;
import org.gcube.application.geoportal.common.model.legacy.UploadedImage;
import org.gcube.application.geoportal.common.model.legacy.WorkspaceContent;
import org.gcube.application.geoportal.common.model.legacy.report.ValidationReport;
import org.gcube.application.geoportal.common.model.legacy.report.ValidationReport.ValidationStatus;
import org.gcube.application.geoportal.common.rest.TempFile;
import org.gcube.application.geoportal.common.utils.Files;
import org.gcube.application.geoportal.model.fault.ConfigurationException;
import org.gcube.application.geoportal.model.fault.PublishException;
import org.gcube.application.geoportal.service.engine.ImplementationProvider;
import org.gcube.application.geoportal.service.engine.SDIManager;
import org.gcube.application.geoportal.service.engine.StorageClientProvider;
import org.gcube.application.geoportal.service.engine.WorkspaceManager;
import org.gcube.application.geoportal.service.engine.WorkspaceManager.FileOptions;
import org.gcube.application.geoportal.service.engine.WorkspaceManager.FolderOptions;
import org.gcube.application.geoportal.service.model.internal.faults.InvalidStateException;
import org.gcube.application.geoportal.service.model.internal.faults.SDIInteractionException;
import org.gcube.application.geoportal.service.utils.Serialization;
import org.gcube.common.storagehub.client.dsl.FolderContainer;
import org.gcube.common.storagehub.model.exceptions.StorageHubException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.mongodb.client.MongoDatabase;

import lombok.Synchronized;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ConcessioniMongoManager extends MongoManager{



	public ConcessioniMongoManager() throws ConfigurationException {
		super();
	}
	private static final String collectionName="legacyConcessioni";
	private static final String DB_NAME="gna_dev";


	private MongoDatabase db=null;

	@Override
	@Synchronized
	protected MongoDatabase getDatabase() {
		if(db==null) {
			db=client.getDatabase(DB_NAME);
		}
		return db;
	}

	protected static Document asDocument (Concessione c) throws JsonProcessingException {
		Document toReturn=Document.parse(Serialization.write(c));
		if(c.getMongo_id()!=null&&!c.getMongo_id().isEmpty())
			toReturn.append(ID, asId(c.getMongo_id()));
		return toReturn;
	}

	protected static Concessione asConcessione (Document d) throws JsonProcessingException, IOException {
		return Serialization.read(d.toJson(), Concessione.class);
	}


	// *** PUBLIC METHODS


	public Concessione registerNew(Concessione toRegister) throws IOException {
		log.trace("Registering {} ",toRegister);
		toRegister.setDefaults();
		ObjectId id=insert(asDocument(toRegister), collectionName);

		Concessione toReturn=asConcessione(getById(id,collectionName));
		toReturn.setMongo_id(asString(id));

		return asConcessione(replace(asDocument(toReturn),collectionName));
	}

	public Concessione replace(Concessione toRegister) throws IOException {
		log.trace("Replacing {} ",toRegister);
		toRegister.setDefaults();
		return asConcessione(replace(asDocument(toRegister),collectionName));
	}

	public Concessione update(String id,String json) throws IOException {
		log.trace("Updating id {} with {} ",id,json);
		Concessione toReturn=asConcessione(update(asId(id),asDoc(json),collectionName));
		log.debug("Refreshing defaults..");
		toReturn.setDefaults();
		return asConcessione(replace(asDocument(toReturn),collectionName));
	}

	
	
	public List<Concessione> list(){
		ArrayList<Concessione> toReturn=new ArrayList<>();
		iterate(null, collectionName).forEach(
				new Consumer<Document>() {
					@Override
					public void accept(Document d) {
						try {
							toReturn.add(asConcessione(d));
						}catch(Throwable t) {
							log.error("Unable to read Document as concessione ",t);
							log.debug("Document was "+d.toJson());				
						}
					}
				});
		return toReturn;
	}

	public Concessione getById(String id) throws JsonProcessingException, IOException {
		log.debug("Loading by ID "+id);
		return asConcessione(getById(asId(id),collectionName));
	}
	public void deleteById(String id) {
		delete(asId(id), collectionName);
	}

	public Concessione publish(String id) throws JsonProcessingException, IOException, InvalidStateException{
		Concessione toReturn=asConcessione(getById(asId(id),collectionName));
		toReturn.setDefaults();
		toReturn.validate();

		// MATERIALIZE LAYERS
		toReturn=publish(toReturn);
		//		replace(asDocument(toReturn),collectionName);

		// CREATE INDEXES
		toReturn=index(toReturn);
		//		replace(asDocument(toReturn),collectionName);

		return asConcessione(replace(asDocument(toReturn),collectionName));
	}


	public Concessione persistContent(String id, String destinationPath, List<TempFile> files) throws Exception{
		log.info("Persisting {} files for path {} in concessione ",files.size(),destinationPath,id);
		try{
			Concessione c = getById(id);
			WorkspaceManager ws=new WorkspaceManager();
			//Check Init Base folder
			FolderContainer baseFolder=null;
			if(c.getFolderId()==null) {					
				String folderName=Files.fixFilename("mConcessione"+"_"+c.getNome()+"_"+Serialization.FULL_FORMATTER.format(LocalDateTime.now()));
				log.info("Creating folder {} for Concessione ID {} ",folderName,id);
				FolderContainer folder=ws.createFolder(new FolderOptions(folderName, "Base Folder for "+c.getNome(),null));
				c.setFolderId(folder.getId());
			}

			log.debug("Folder id is : "+c.getFolderId());
			baseFolder=ws.getFolderById(c.getFolderId());

			AssociatedContent section=c.getContentByPath(destinationPath);
			log.debug("Found section {} for path {}",section,destinationPath);
			store(section,files,ws,baseFolder);
			log.debug("Updating dafults for {} ",c);
			c.setDefaults();
			return asConcessione(replace(asDocument(c),collectionName));
		}catch(Exception e) {
			throw new Exception("Unable to save file.",e);
		}
	}

	private static Concessione index(Concessione record) {
		log.info("Indexing {} ",record.getId());
		ValidationReport report= new ValidationReport("Index Report ");
		PostgisIndex index;
		try {
			index = new PostgisIndex();
			index.registerCentroid(record);
			report.addMessage(ValidationStatus.PASSED, "Registered centroid");
		} catch (SDIInteractionException | PublishException | SQLException | ConfigurationException e) {
			log.error("Unable to index {} ",record,e);
			report.addMessage(ValidationStatus.WARNING, "Internal error while indexing.");
		}
		return record;
	}





	private static Concessione publish(Concessione conc) {

		// CHECK CONDITION BY PROFILE


		log.debug("Publishing "+conc.getNome());

		ValidationReport report=new ValidationReport("Publish report");
		try {
			SDIManager sdiManager=new SDIManager();
			ArrayList<AssociatedContent> list=new ArrayList<AssociatedContent>(); 

			//Concessione
			String workspace= sdiManager.createWorkspace("gna_conc_"+conc.getMongo_id());
			list.add(conc.getPosizionamentoScavo());
			list.addAll(conc.getPianteFineScavo());

			for(AssociatedContent c:list) {
				if(c instanceof LayerConcessione) {
					try {
						List<PersistedContent> p=c.getActualContent();

						GeoServerContent geoserverPersisted=sdiManager.pushShapeLayerFileSet((SDILayerDescriptor)c, workspace, conc.getMongo_id());
						//						geoserverPersisted.setAssociated(c);


						p.add(geoserverPersisted);
						c.setActualContent(p);
					}catch(SDIInteractionException e) {
						log.warn("Unable to publish layers.",e);
						report.addMessage(ValidationStatus.WARNING, "Layer "+c.getTitolo()+" non pubblicato.");
					}
					report.addMessage(ValidationStatus.PASSED, "Pubblicato layer "+c.getTitolo());
				}
			}


		} catch (SDIInteractionException e1) {
			report.addMessage(ValidationStatus.WARNING, "Unable to publish layers "+e1.getMessage());
		}

		conc.setReport(report);
		return conc;
	}

	private static final void store(AssociatedContent content,List<TempFile> files, WorkspaceManager ws, FolderContainer base) throws Exception {
		FolderContainer sectionParent=null;		

		if(content instanceof RelazioneScavo)
			sectionParent = ws .createFolder(new FolderOptions(
					"relazione","Relazione di scavo : "+content.getTitolo(),base));

		else if (content instanceof UploadedImage) 
			sectionParent = ws .createFolder(new FolderOptions(
					"imgs","Immagini rappresentative : "+content.getTitolo(),base));

		else if (content instanceof SDILayerDescriptor)
			//SDI Section
			if(content instanceof LayerConcessione) 
				sectionParent = ws .createFolder(new FolderOptions(
						content.getTitolo(),"Layer Concessione : "+content.getTitolo(),ws.getSubFolder(base,"layers")));
			else throw new Exception("Invalid SDI Content "+content);
		else if (content instanceof OtherContent ) 
			sectionParent = ws .createFolder(new FolderOptions(
					content.getTitolo(),"Relazione di scavo : "+content.getTitolo(),ws.getSubFolder(base,"other")));
		else throw new Exception("Invalid Content "+content);

		content.setActualContent(new ArrayList<PersistedContent>());
		StorageClientProvider storage=ImplementationProvider.get().getStorageProvider();
		for(TempFile f : files) {
			WorkspaceContent wsContent=ws.storeToWS(new FileOptions(f.getFilename(), storage.open(f.getId()), "Imported via GeoPortal", sectionParent));
			log.debug("Registered "+wsContent+" for "+content);
			content.getActualContent().add(wsContent);
		}
		content.setMongo_id(asString(new ObjectId()));
	}

}
