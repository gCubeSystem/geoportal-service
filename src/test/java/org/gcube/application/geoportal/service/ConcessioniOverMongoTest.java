package org.gcube.application.geoportal.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.gcube.application.geoportal.common.model.legacy.Concessione;
import org.gcube.application.geoportal.common.model.legacy.Concessione.Paths;
import org.gcube.application.geoportal.common.model.legacy.LayerConcessione;
import org.gcube.application.geoportal.common.model.legacy.report.ValidationReport.ValidationStatus;
import org.gcube.application.geoportal.common.rest.AddSectionToConcessioneRequest;
import org.gcube.application.geoportal.common.rest.InterfaceConstants;
import org.gcube.application.geoportal.common.rest.TempFile;
import org.gcube.application.geoportal.common.utils.Files;
import org.gcube.application.geoportal.common.utils.StorageUtils;
import org.gcube.application.geoportal.service.legacy.TokenSetter;
import org.gcube.application.geoportal.service.utils.Serialization;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ConcessioniOverMongoTest extends BasicServiceTestUnit{


	private static final String PATH=InterfaceConstants.Methods.MONGO_CONCESSIONI;

	private static final String PUBLISH_PATH=InterfaceConstants.Methods.PUBLISH_PATH;
	private static final String FILES_PATH=InterfaceConstants.Methods.REGISTER_FILES_PATH;


	@Before
	public void setContext() {
		TokenSetter.set("/gcube/devsec/devVRE");
	}


	private static Concessione upload(WebTarget target,String id, String path, String ...files) throws Exception {
		ArrayList<TempFile> array=new ArrayList<TempFile>();
		for(String file:files)
			array.add(new StorageUtils().putOntoStorage(new FileInputStream(
					Files.getFileFromResources("concessioni/"+file)), file));
		
		
		AddSectionToConcessioneRequest request=new AddSectionToConcessioneRequest();
		request.setDestinationPath(path);

		request.setStreams(array);

		return check(target.path(FILES_PATH).path(id).request(MediaType.APPLICATION_JSON).post(Entity.entity(Serialization.write(request), MediaType.APPLICATION_JSON)),Concessione.class);
		
	}

	
	private static Concessione publish(WebTarget target, Concessione conc) throws Exception {
		Response resp=target.path(PUBLISH_PATH).path(conc.getMongo_id()).request(MediaType.APPLICATION_JSON).
		put(Entity.entity(Serialization.write(conc), MediaType.APPLICATION_JSON));
		return check(resp,Concessione.class);
	}
	private static Concessione register(WebTarget target, Concessione c) throws Exception {
		Response resp=target.request(MediaType.APPLICATION_JSON).post(Entity.entity(Serialization.write(c), MediaType.APPLICATION_JSON));
		return check(resp,Concessione.class);
	}
	
	private static Concessione get(WebTarget target) throws Exception {
		return register(target,TestModel.prepareConcessione());		
	}
	
	// ********** TESTS 
	
	@Test
	public void list() {
		WebTarget target=target(PATH);
		System.out.println(target.request(MediaType.APPLICATION_JSON).get(List.class));
	}

	@Test
	public void createNew() throws Exception {
		WebTarget target=target(PATH);
		Concessione c=register(target,TestModel.prepareConcessione());
		Assert.assertTrue(c.getMongo_id()!=null&&!c.getMongo_id().isEmpty());
	}
	
	
	@Test
	public void delete() throws Exception {
		WebTarget target=target(PATH);
		Concessione c = get(target);
		check(target.path(c.getMongo_id()).request(MediaType.APPLICATION_JSON).delete(),null);
	}


	@Test
	public void getById() throws Exception {
		WebTarget target=target(PATH);
		Concessione c = get(target);
		Response resp=target.path(c.getMongo_id()).request(MediaType.APPLICATION_JSON).get();
		Concessione loaded=check(resp,Concessione.class);
		Assert.assertTrue(loaded.getMongo_id()!=null&&!loaded.getMongo_id().isEmpty());
		System.out.println("Got by ID "+loaded);
	}


	@Test
	public void update() throws Exception {
		WebTarget target=target(PATH);	
		Concessione c = get(target);
		String newTitle="Questo titolo l'ho modificato mo nel test quello proprio apposta pewr questa cosa'";
		c.setNome(newTitle);
		Response resp=target.request(MediaType.APPLICATION_JSON).put(Entity.entity(Serialization.write(c), MediaType.APPLICATION_JSON));
		Assert.assertTrue(check(resp,Concessione.class).getNome().equals(newTitle));
	}

	@Test
	public void uploadFile() throws Exception {
		WebTarget target=target(PATH);
		Response resp=target.request(MediaType.APPLICATION_JSON).post(Entity.entity(Serialization.write(TestModel.prepareEmptyConcessione()), MediaType.APPLICATION_JSON));
		Concessione c=check(resp,Concessione.class);
		Assert.assertTrue(c.getMongo_id()!=null&&!c.getMongo_id().isEmpty());
		System.out.println("ID IS "+c.getMongo_id());

		// Insert section
		c.setRelazioneScavo(TestModel.prepareConcessione().getRelazioneScavo());
		//		c.getRelazioneScavo().setMongo_id(TestModel.rnd());

		resp=target.request(MediaType.APPLICATION_JSON).put(Entity.entity(Serialization.write(c), MediaType.APPLICATION_JSON));

		
		
		
		c=upload(target,c.getMongo_id(),Paths.RELAZIONE,"relazione.pdf");
		assertNotNull(c.getRelazioneScavo().getActualContent());
		assertTrue(c.getRelazioneScavo().getActualContent().size()>0);

		System.out.println("File is "+c.getRelazioneScavo().getActualContent().get(0));
	}

	
	
	@Test
	public void publish() throws Exception {
		WebTarget target=target(PATH);
		Concessione c=TestModel.prepareConcessione(1,2);
		
		c.setNome("Concessione : publish test");
		
		
		
		// Register new 
		c=register(target,c);		
		
		//Upload files
		upload(target,c.getMongo_id(),Paths.RELAZIONE,"relazione.pdf");
		upload(target,c.getMongo_id(),Paths.POSIZIONAMENTO,"pos.shp","pos.shx");
		
		// Clash on workspaces
		upload(target,c.getMongo_id(),Paths.piantaByIndex(0),"pianta.shp","pianta.shx");
		upload(target,c.getMongo_id(),Paths.imgByIndex(0),"immagine.png");
		upload(target,c.getMongo_id(),Paths.imgByIndex(1),"immagine2.png");

		
		
		// Immagini
		Concessione published=publish(target, c);
		System.out.println("Published : "+published);
		System.out.println("Report is : "+published.getReport());
		assertNotNull(published.getReport());
		assertEquals(ValidationStatus.PASSED,published.getReport().getStatus());
		
		assertEquals(published.getImmaginiRappresentative().size(),2);
		assertEquals(published.getPianteFineScavo().size(),1);
		assertNotNull(published.getPosizionamentoScavo().getWmsLink());
		for(LayerConcessione l : published.getPianteFineScavo())
			assertNotNull(l.getWmsLink());
		assertNotNull(published.getCentroidLat());
		assertNotNull(published.getCentroidLong());
	}
	
	
	
	
}
