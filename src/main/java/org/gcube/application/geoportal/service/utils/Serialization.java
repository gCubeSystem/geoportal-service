package org.gcube.application.geoportal.service.utils;

import java.io.IOException;
import java.time.format.DateTimeFormatter;

import org.gcube.application.geoportal.model.Record;
import org.gcube.application.geoportal.model.concessioni.Concessione;
import org.gcube.application.geoportal.model.concessioni.LayerConcessione;
import org.gcube.application.geoportal.model.content.AssociatedContent;
import org.gcube.application.geoportal.model.content.OtherContent;
import org.gcube.application.geoportal.model.content.PersistedContent;
import org.gcube.application.geoportal.model.content.UploadedImage;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

public class Serialization {

	
	public static final DateTimeFormatter FULL_FORMATTER=DateTimeFormatter.ofPattern("uuuuMMdd_HH-mm-ss");
	
	public static ObjectMapper mapper;
		
		static {
			mapper=new ObjectMapper();
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES,false);
			mapper.registerModule(new JavaTimeModule());			
		}
		
		public static <T> T read(String jsonString,Class<T> clazz) throws JsonProcessingException, IOException {
			return mapper.readerFor(clazz).readValue(jsonString);
		}
		
		public static String write(Object toWrite) throws JsonProcessingException {
			if(toWrite instanceof Concessione)
				detach((Concessione) toWrite);
			
			
			
			String toReturn= mapper.writeValueAsString(toWrite);
			
			if(toWrite instanceof Concessione)
				reattach((Concessione) toWrite);
			return toReturn;
		}
		

		
		// Avoid infiniteLoop in JPA
		private static void detach(Concessione c) {
			if (c!=null) {
				detach(c.getRelazioneScavo());
				detach(c.getPosizionamentoScavo());
				if(c.getPianteFineScavo()!=null)
					c.getPianteFineScavo().forEach((LayerConcessione l)->{detach(l);});
				if(c.getImmaginiRappresentative()!=null)
					c.getImmaginiRappresentative().forEach(((UploadedImage u)->{detach(u);}));
				if(c.getGenericContent()!=null)
					c.getGenericContent().forEach(((OtherContent u)->{detach(u);}));
			}
		}
		
		private static void detach(AssociatedContent a) {
			if(a!=null) {
				a.setRecord(null);
				if(a.getActualContent()!=null)
					a.getActualContent().forEach((PersistedContent p)->{p.setAssociated(null);});
			}
		}
		
		
		private static void reattach(Concessione c) {
			if(c!=null) {
				reattach(c.getRelazioneScavo(),c);
				reattach(c.getPosizionamentoScavo(),c);
				if(c.getPianteFineScavo()!=null)
					c.getPianteFineScavo().forEach((LayerConcessione l)->{reattach(l,c);});
				if(c.getImmaginiRappresentative()!=null)
					c.getImmaginiRappresentative().forEach(((UploadedImage u)->{reattach(u,c);}));
				if(c.getGenericContent()!=null)
					c.getGenericContent().forEach(((OtherContent u)->{reattach(u,c);}));
			}
		}
		
		private static void reattach(AssociatedContent a,Record r) {
			if(a!=null) {
				a.setRecord(r);
				if(a.getActualContent()!=null)
					a.getActualContent().forEach((PersistedContent p)->{p.setAssociated(a);});
			}
		}
}
