package org.gcube.application.geoportal.service.engine;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.Duration;
import java.time.temporal.ChronoUnit;

import org.eclipse.persistence.internal.sessions.remote.SequencingFunctionCall.GetNextValue;
import org.gcube.application.geoportal.common.rest.InterfaceConstants;
import org.gcube.application.geoportal.model.fault.ConfigurationException;
import org.gcube.application.geoportal.service.utils.ContextUtils;
import org.gcube.contentmanagement.blobstorage.service.IClient;
import org.gcube.contentmanagement.blobstorage.transport.backend.RemoteBackendException;
import org.gcube.contentmanager.storageclient.wrapper.AccessType;
import org.gcube.contentmanager.storageclient.wrapper.MemoryType;
import org.gcube.contentmanager.storageclient.wrapper.StorageClient;
import org.gcube.data.transfer.library.utils.Utils;

import lombok.extern.slf4j.Slf4j;
@Slf4j
public class StorageClientProvider extends AbstractScopedMap<IClient>{

	
	
	public StorageClientProvider() {
		super("Storage client cache");
		setTTL(Duration.of(10, ChronoUnit.MINUTES));
	}
	
	@Override
	protected IClient retrieveObject() throws ConfigurationException {
		return new StorageClient(InterfaceConstants.SERVICE_CLASS, InterfaceConstants.SERVICE_NAME, ContextUtils.getCurrentCaller(), AccessType.SHARED, MemoryType.VOLATILE).getClient();
	}
	
	@Override
	protected void dispose(IClient toDispose) {
		try {	
			toDispose.close();
		}catch (NullPointerException e) {
			// expected if closed without uploading
		}catch(Throwable t) {
			log.warn(" unable to dispose "+toDispose,t); 
		}
	}

	@Override
	public void init() {
		
	}
	
	
	//wrapping methods 
	
	public InputStream open(String id) throws MalformedURLException, RemoteBackendException, IOException, ConfigurationException {
		return new URL(getObject().getHttpsUrl().RFileById(id)).openConnection().getInputStream();
	}
	
	public String store(InputStream is) throws RemoteBackendException, ConfigurationException {
		return getObject().put(true).LFile(is).RFile(Utils.getUniqueString());		
	}
	
	
}
