package org.gcube.application.geoportal.service.model.internal.db;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class MongoConnection {

	private String user;
	private String password;
	private String database;
	private List<String> hosts=new ArrayList<String>();
	private int port;
}
