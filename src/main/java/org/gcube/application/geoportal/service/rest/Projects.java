package org.gcube.application.geoportal.service.rest;

import java.util.Collections;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;

import org.bson.Document;
import org.gcube.application.geoportal.common.model.project.Project;
import org.gcube.application.geoportal.common.rest.InterfaceConstants;

import lombok.extern.slf4j.Slf4j;

@Path(InterfaceConstants.Methods.PROJECTS)
@Slf4j
public class Projects {

	//***************** GENERIC PROJECTS
	// GET ALL
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Project> getAll() {
		return new GuardedMethod<List<Project>>() {
			protected List<Project> run() throws Exception ,WebApplicationException {
				return Collections.singletonList(new Project());
			};
		}.execute().getResult();
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/search")
	public List<Project> getFilteredAll(Document filter){
		return new GuardedMethod<List<Project>>() {
			protected List<Project> run() throws Exception ,WebApplicationException {
				return Collections.singletonList(new Project());
			};
		}.execute().getResult();
	}


	//***************** BY PROFILE ID

	// Create new Project
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("{"+InterfaceConstants.Parameters.PROFILE_ID+"}")
	public Project registerNew(@PathParam(InterfaceConstants.Parameters.PROFILE_ID)String profileId,
			Document toRegister) {
		return new GuardedMethod<Project>() {
			@Override
			protected Project run() throws Exception, WebApplicationException {
				return new Project();
			}
		}.execute().getResult();
	}


	// GET ALL (Filters apply)
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/search/{"+InterfaceConstants.Parameters.PROFILE_ID+"}")
	public List<Project> getFilteredAllInProfiles(@PathParam(InterfaceConstants.Parameters.PROFILE_ID)String profileId,
			Document filters) {
		return new GuardedMethod<List<Project>>() {
			protected List<Project> run() throws Exception ,WebApplicationException {
				return Collections.singletonList(new Project());
			};
		}.execute().getResult();
	}

	// GET ALL
	@GET
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("{"+InterfaceConstants.Parameters.PROFILE_ID+"}")
	public List<Project> getAllinProfile(@PathParam(InterfaceConstants.Parameters.PROFILE_ID)String profileId) {
		return new GuardedMethod<List<Project>>() {
			protected List<Project> run() throws Exception ,WebApplicationException {
				return Collections.singletonList(new Project());
			};
		}.execute().getResult();
	}


	//***************** BY PROFILE ID + PROJECT ID 
	// GET BY ID
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("{"+InterfaceConstants.Parameters.PROFILE_ID+"}/{"+InterfaceConstants.Parameters.PROJECT_ID+"}")
	public Project getByID(@PathParam(InterfaceConstants.Parameters.PROFILE_ID) String profile,
			@PathParam(InterfaceConstants.Parameters.PROJECT_ID) String id) {
		Project toReturn=new GuardedMethod<Project>() {
			@Override
			protected Project run() throws Exception ,WebApplicationException{
				return new Project();
			}
		}.execute().getResult();
		return toReturn;
	}

	
	// DELETE BY ID
		@DELETE
		@Produces(MediaType.APPLICATION_JSON)
		@Path("{"+InterfaceConstants.Parameters.PROFILE_ID+"}/{"+InterfaceConstants.Parameters.PROJECT_ID+"}")
		public void delete(@PathParam(InterfaceConstants.Parameters.PROFILE_ID) String profile,
				@PathParam(InterfaceConstants.Parameters.PROJECT_ID) String id) {
			new GuardedMethod<Project>() {
				@Override
				protected Project run() throws Exception ,WebApplicationException{
					// TODO DELETE
					return null;
				}
			}.execute().getResult();
		}
	
	@PUT	
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("{"+InterfaceConstants.Parameters.PROFILE_ID+"}/{"+InterfaceConstants.Parameters.PROJECT_ID+"}")
	public Project updateDocument(@PathParam(InterfaceConstants.Parameters.PROFILE_ID) String profile,
			@PathParam(InterfaceConstants.Parameters.PROJECT_ID) String id,Document toSetDocument) {
		Project toReturn=new GuardedMethod<Project>() {
			@Override
			protected Project run() throws Exception ,WebApplicationException{
				return new Project();
			}
		}.execute().getResult();
		return toReturn;
	}
}
