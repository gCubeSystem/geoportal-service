package org.gcube.application.geoportal.service.legacy;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Collection;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.gcube.application.geoportal.common.rest.InterfaceConstants;
import org.gcube.application.geoportal.common.utils.Files;
import org.gcube.application.geoportal.model.concessioni.Concessione;
import org.gcube.application.geoportal.model.concessioni.LayerConcessione;
import org.gcube.application.geoportal.model.content.AssociatedContent;
import org.gcube.application.geoportal.model.content.UploadedImage;
import org.gcube.application.geoportal.model.fault.ConfigurationException;
import org.gcube.application.geoportal.model.report.PublicationReport;
import org.gcube.application.geoportal.model.report.ValidationReport.ValidationStatus;
import org.gcube.application.geoportal.service.BasicServiceTestUnit;
import org.gcube.application.geoportal.service.engine.ImplementationProvider;
import org.gcube.application.geoportal.service.model.internal.rest.AddSectionToConcessioneRequest;
import org.gcube.application.geoportal.service.model.internal.rest.AddSectionToConcessioneRequest.SHUBFileDescriptor;
import org.gcube.application.geoportal.service.model.internal.rest.AddSectionToConcessioneRequest.Section;
import org.gcube.application.geoportal.service.utils.Serialization;
import org.gcube.com.fasterxml.jackson.core.JsonProcessingException;
import org.gcube.contentmanagement.blobstorage.transport.backend.RemoteBackendException;
import org.junit.Test;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ConcessioniTest extends BasicServiceTestUnit {

	
	
	
	@Test
	public void readId() {
		WebTarget target=target(InterfaceConstants.Methods.CONCESSIONI);
		try{
			System.out.println(target.path("9").request(MediaType.APPLICATION_JSON).get(String.class));
		}catch(WebApplicationException e) {
			if(e.getResponse().getStatus()==404)
				System.out.println("Object not found");
			else throw e;
		}
	}
	
	@Test
	public void list() throws JsonProcessingException, IOException{
		WebTarget target=target(InterfaceConstants.Methods.CONCESSIONI);
		System.err.println(target.getUri());		
		
		Collection coll=target.request(MediaType.APPLICATION_JSON).get(Collection.class);
		System.out.println("Size of collection is "+coll.size());
		System.out.println("Iterating through objects.. ");
		for(Object o:coll) {
			System.out.println("Object class is "+o.getClass());
			Concessione c=Serialization.read(o.toString(), Concessione.class);
			System.out.println("Concessione is : "+Serialization.write(c));
		}
		
	}
	
	@Test
	public void failPublish() throws com.fasterxml.jackson.core.JsonProcessingException, IOException {
		Concessione toCreate=OLDTestModel.prepareEmptyConcessione();
		
		Concessione conc=pushConcessione(toCreate);
		System.out.println(publish(conc.getId()+"").prettyPrint());
	}
	
	
	
	@Test
	public void createNew() throws IOException {
		Concessione toCreate=OLDTestModel.prepareEmptyConcessione();
		
		pushConcessione(toCreate);
		
	}
	@Test
	public void publishNew() throws IOException, RemoteBackendException, ConfigurationException {
		Concessione toCreate=OLDTestModel.prepareEmptyConcessione();
		Concessione registered = pushConcessione(toCreate);
		System.out.println("Registered at "+Serialization.write(registered));
		Concessione fullTemplate=OLDTestModel.prepareConcessione();
		
		//Push Relazione
		publishSection(registered.getId()+"",formRequest(Section.RELAZIONE,fullTemplate.getRelazioneScavo(),"concessioni/relazione.pdf"));
		assertNotNull(getById(registered.getId()+"").getRelazioneScavo());
		
		//Push Immagini
		for(UploadedImage img:fullTemplate.getImmaginiRappresentative())
			publishSection(registered.getId()+"",formRequest(Section.UPLOADED_IMG,img,"concessioni/immagine.png"));
		assertNotNull(getById(registered.getId()+"").getImmaginiRappresentative());
		assertTrue(getById(registered.getId()+"").getImmaginiRappresentative().size()==fullTemplate.getImmaginiRappresentative().size());
		
		//Push Posizinamento
		publishSection(registered.getId()+"",formRequest(Section.POSIZIONAMENTO,fullTemplate.getPosizionamentoScavo(),"concessioni/pos.dbf","concessioni/pos.shp"));
		assertNotNull(getById(registered.getId()+"").getPosizionamentoScavo());
		
		//Push piante
		for(LayerConcessione l:fullTemplate.getPianteFineScavo())
			publishSection(registered.getId()+"",formRequest(Section.PIANTA,l,"concessioni/pos.dbf","concessioni/pos.shp"));
		assertNotNull(getById(registered.getId()+"").getPianteFineScavo());
		assertTrue(getById(registered.getId()+"").getPianteFineScavo().size()==fullTemplate.getPianteFineScavo().size());
		
		
		
		Concessione reloaded = getById(registered.getId()+"");
		System.out.println("Goind to publish "+Serialization.write(reloaded));
		PublicationReport report=publish(registered.getId()+"");
		System.out.println("REPORT IS "+report.prettyPrint()+"");
//		System.out.println("Concessione is "+report.getTheRecord().asJson());
		assertTrue(report.getStatus().equals(ValidationStatus.PASSED));
	}
	
	
	// ACTUAL METHODS
	
	
	private PublicationReport  publish(String id) throws com.fasterxml.jackson.core.JsonProcessingException, IOException {
		WebTarget target=target(InterfaceConstants.Methods.CONCESSIONI);
		Response resp=target.path("publish").path(id).request(MediaType.APPLICATION_JSON).put(Entity.entity("sopmething", MediaType.APPLICATION_JSON));
		if(resp.getStatus()<200||resp.getStatus()>=300)
			System.err.println("RESP STATUS IS "+resp.getStatus());
		String resString=resp.readEntity(String.class);
		System.out.println("Resp String is "+resString);
		PublicationReport registered=Serialization.read(resString, PublicationReport.class);
		System.out.println("Registered concessione at : "+registered);
		return registered;
	}
	
	private AddSectionToConcessioneRequest formRequest(Section section,AssociatedContent content,String... files) throws RemoteBackendException, FileNotFoundException, ConfigurationException {
		AddSectionToConcessioneRequest toReturn=new AddSectionToConcessioneRequest();		
		toReturn.setSection(section);
		toReturn.setToRegister(content);
		for(String f:files) {
			SHUBFileDescriptor desc=new SHUBFileDescriptor();
			desc.setFilename(f.substring(f.lastIndexOf("/")));
			String sId=ImplementationProvider.get().getStorageProvider().store(
					new FileInputStream(Files.getFileFromResources(f)));
			desc.setShubID(sId);
			toReturn.getStreams().add(desc);
		}
		return toReturn;
	}
	
	private Response publishSection(String id, AddSectionToConcessioneRequest request) {
		WebTarget target=target(InterfaceConstants.Methods.CONCESSIONI);
		Response resp=target.path("section").path(id).request(MediaType.APPLICATION_JSON).put(Entity.entity(request, MediaType.APPLICATION_JSON));
		if(resp.getStatus()<200||resp.getStatus()>=300) {
			System.err.println("RESP STATUS IS "+resp.getStatus());
			System.err.println("RESP IS "+resp.readEntity(String.class));
		}
		return resp;
	}
	
	private Concessione pushConcessione(Concessione c) throws com.fasterxml.jackson.core.JsonProcessingException, IOException {
		WebTarget target=target(InterfaceConstants.Methods.CONCESSIONI);
		Response resp=target.request(MediaType.APPLICATION_JSON).put(Entity.entity(Serialization.write(c), MediaType.APPLICATION_JSON));
		String resString=resp.readEntity(String.class);
		System.out.println("Resp String is "+resString);
		Concessione registered=Serialization.read(resString, Concessione.class);
		System.out.println("Registered concessione at : "+registered);
		return registered;
	}
	
	private Concessione getById(String id) throws com.fasterxml.jackson.core.JsonProcessingException, IOException {
		WebTarget target=target(InterfaceConstants.Methods.CONCESSIONI);
		Response resp=target.path(id).request(MediaType.APPLICATION_JSON).get();
		if(resp.getStatus()<200||resp.getStatus()>=300)
			System.err.println("RESP STATUS IS "+resp.getStatus());
		String resString=resp.readEntity(String.class);
		System.out.println("Resp String is "+resString);
		return Serialization.read(resString, Concessione.class);
	}
	
	
}
