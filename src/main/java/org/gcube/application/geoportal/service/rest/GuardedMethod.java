package org.gcube.application.geoportal.service.rest;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response.Status;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public abstract class GuardedMethod<T> {

	
	private T result=null;
	
	public GuardedMethod<T> execute() throws WebApplicationException{
		try {
			result=run();
			return this;
		}catch(WebApplicationException e) {
			throw e;
		}catch(Throwable t) {
			log.error("Unexpected error ",t);
			throw new WebApplicationException("Unexpected internal error", t,Status.INTERNAL_SERVER_ERROR);
		}
		
	}
	
	public T getResult() {
		return result;
	}
	
	
	protected abstract T run() throws Exception,WebApplicationException;
}
