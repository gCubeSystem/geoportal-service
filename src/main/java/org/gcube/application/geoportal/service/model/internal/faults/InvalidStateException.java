package org.gcube.application.geoportal.service.model.internal.faults;

public class InvalidStateException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8926481061304048080L;

	public InvalidStateException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public InvalidStateException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public InvalidStateException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public InvalidStateException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public InvalidStateException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	
	
}
