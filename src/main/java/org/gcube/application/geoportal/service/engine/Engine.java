package org.gcube.application.geoportal.service.engine;

import org.gcube.application.geoportal.model.fault.ConfigurationException;

public interface Engine <T> {

	public void init();
	public void shustdown();
	
	public T getObject() throws ConfigurationException;
}
