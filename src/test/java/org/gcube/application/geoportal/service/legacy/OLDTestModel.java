package org.gcube.application.geoportal.service.legacy;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;

import org.gcube.application.geoportal.model.AccessPolicy;
import org.gcube.application.geoportal.model.concessioni.Concessione;
import org.gcube.application.geoportal.model.concessioni.LayerConcessione;
import org.gcube.application.geoportal.model.concessioni.RelazioneScavo;
import org.gcube.application.geoportal.model.content.UploadedImage;

public class OLDTestModel {

	public static Concessione prepareEmptyConcessione() {
		Concessione concessione=new Concessione();

		// Generic fields

			
		// Concessione fields

		concessione.setNome("Italia, forse");
		concessione.setIntroduzione("This is my project");
		concessione.setDescrizioneContenuto("It contains this and that");

		concessione.setAuthors(Arrays.asList(new String[] {"Some one","Some, oneelse"}));

		concessione.setContributore("Contrib 1");
		concessione.setTitolari(Arrays.asList(new String[] {"Some one","Some, oneelse"}));
		concessione.setResponsabile("Someone");
		concessione.setEditore("Editore");

		concessione.setFontiFinanziamento(Arrays.asList(new String[] {"Big pharma","Pentagon"}));


		concessione.setSoggetto(Arrays.asList(new String[] {"Research Excavation","Archeology"}));


		concessione.setDataInizioProgetto(LocalDateTime.now());
		concessione.setDataFineProgetto(LocalDateTime.now());

		concessione.setLicenzaID("CC-BY");
		
		concessione.setTitolareLicenza(Arrays.asList(new String[] {"Qualcun altro"}));
		concessione.setTitolareCopyright(Arrays.asList(new String[] {"Chiedilo in giro"}));

		concessione.setParoleChiaveLibere(Arrays.asList(new String[] {"Robba","Stuff"}));
		concessione.setParoleChiaveICCD(Arrays.asList(new String[] {"vattelapesca","somthing something"}));


		concessione.setCentroidLat(43.0); //N-S
		concessione.setCentroidLong(9.0); //E-W
		
		return concessione;
	}
	
	public static Concessione prepareConcessione() {
		
		Concessione concessione=prepareEmptyConcessione();

		
		
		// Attachments

		// Relazione scavo
		RelazioneScavo relScavo=new RelazioneScavo();

		relScavo.setAbstractSection("simple abstract section");
		relScavo.setResponsabili(concessione.getAuthors());

		concessione.setRelazioneScavo(relScavo);
		//Immagini rappresentative
		ArrayList<UploadedImage> imgs=new ArrayList<>();
		for(int i=0;i<5;i++) {
			UploadedImage img=new UploadedImage();
			img.setTitolo("My image number "+i);
			img.setDidascalia("You can see my image number "+i);
			img.setFormat("TIFF");
			img.setCreationTime(LocalDateTime.now());
			img.setResponsabili(concessione.getAuthors());
			imgs.add(img);
		}
		concessione.setImmaginiRappresentative(imgs);
		//Posizionamento
		LayerConcessione posizionamento=new LayerConcessione();
		posizionamento.setValutazioneQualita("Secondo me si");
		posizionamento.setMetodoRaccoltaDati("Fattobbene");
		posizionamento.setScalaAcquisizione("1:10000");
		posizionamento.setAuthors(concessione.getAuthors());	
		concessione.setPosizionamentoScavo(posizionamento);

		// Piante fine scavo
		ArrayList<LayerConcessione> piante=new ArrayList<LayerConcessione>();
		for(int i=0;i<4;i++) {
			LayerConcessione pianta=new LayerConcessione();
			pianta.setValutazioneQualita("Secondo me si");
			pianta.setMetodoRaccoltaDati("Fattobbene");
			pianta.setScalaAcquisizione("1:10000");
			pianta.setAuthors(concessione.getAuthors());	
			pianta.setPolicy(AccessPolicy.RESTRICTED);
			piante.add(pianta);
		}
		concessione.setPianteFineScavo(piante);
		
		return concessione;
	}
}
