package org.gcube.application.geoportal.service.engine.mongo;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.gcube.application.geoportal.common.model.legacy.Concessione;
import org.gcube.application.geoportal.model.db.DBConstants;
import org.gcube.application.geoportal.model.db.PostgisTable;
import org.gcube.application.geoportal.model.db.PostgisTable.Field;
import org.gcube.application.geoportal.model.db.PostgisTable.FieldType;
import org.gcube.application.geoportal.model.fault.ConfigurationException;
import org.gcube.application.geoportal.model.fault.PublishException;
import org.gcube.application.geoportal.service.engine.SDIManager;
import org.gcube.application.geoportal.service.model.internal.faults.SDIInteractionException;
import org.gcube.application.geoportal.service.utils.Serialization;
import org.gcube.application.geoportal.storage.PostgisDBManager;
import org.gcube.application.geoportal.storage.PostgisDBManagerI;

import lombok.Getter;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class PostgisIndex {

	static{
		try {
			Class.forName("org.postgresql.Driver");
			Class.forName("org.postgis.DriverWrapper");
		} catch (Exception var2) {
			throw new RuntimeException(var2);
		}
	}


	private SDIManager sdiManager;
	
	
	public PostgisIndex() throws SDIInteractionException, SQLException, ConfigurationException {
		super();
		this.sdiManager=new SDIManager();
		init();
	}


	protected PostgisTable getCentroidsTable() {
		return DBConstants.Concessioni.CENTROIDS;
	}

	public void init() throws SQLException, ConfigurationException, SDIInteractionException {
		log.debug("Contacting postgis DB .. ");
		PostgisDBManagerI db=PostgisDBManager.get();
		log.debug("Checking if centroids table exists..");
		PostgisTable table=getCentroidsTable();
		db.create(table);
		db.commit();
		sdiManager.configureCentroidLayer("centroids_concessioni", "gna", "gna_postgis",table,db.getConnectionDescriptor());

	}



	public void registerCentroid(Concessione record) throws PublishException{

		try {
			log.debug("Evaluating Centroid");
			Map<String,String> centroidRow=evaluateCentroid(record);

			log.debug("Contacting postgis DB .. ");
			PostgisDBManagerI db=PostgisDBManager.get();

			PostgisTable centroidsTable=getCentroidsTable();
			log.debug("Inserting / updated centroid Row {} ",centroidRow);

			PreparedStatement ps = db.prepareInsertStatement(centroidsTable, true, true);

			log.debug("Deleting centroid if present. ID is "+record.getId());
			db.deleteByFieldValue(centroidsTable, new Field(DBConstants.Concessioni.PRODUCT_ID,FieldType.TEXT), record.getMongo_id());

			centroidsTable.fillCSVPreparedStatament(centroidRow, ps, false);
			ps.executeUpdate();
			db.commit();


		}catch(SQLException e) {
			log.warn("Unable to publish Centroid for record "+record,e);
			throw new PublishException("Unable to publish centroid.",e, null);
		} catch (ConfigurationException e) {
			log.warn("Unable to contact centroids db "+record.getRecordType(),e);
			throw new PublishException("Unable to publish centroid.",e, null);
		}

	}
	


	
	public void removeCentroid(Concessione record) {
		try {
			PostgisDBManagerI db=PostgisDBManager.get();
			PostgisTable centroidsTable=getCentroidsTable();
			log.debug("Deleting centroid if present. ID is "+record.getId());
			db.deleteByFieldValue(centroidsTable, new Field(DBConstants.Concessioni.PRODUCT_ID,FieldType.TEXT), record.getMongo_id());
		}catch(Exception e) {
			log.warn("Unable to remove centroid ",e);
		}
	}

	
	protected static Map<String,String> evaluateCentroid(Concessione record){


		// CENTROID 
		Map<String,String> centroidsRow=new HashMap<String, String>();
		centroidsRow.put(DBConstants.Concessioni.PRODUCT_ID, record.getMongo_id());
		centroidsRow.put(DBConstants.Concessioni.ANNO, record.getDataInizioProgetto().getYear()+"");
		centroidsRow.put(DBConstants.Concessioni.NOME, record.getNome());
		centroidsRow.put(DBConstants.Concessioni.REGIONE, ""); //TODO 
		
		
		
		if(record.getCentroidLat()==null||record.getCentroidLat()==0)
		try {
			log.debug("Evaluating Centroid latitude for record "+record);
			record.setCentroidLat((record.getPosizionamentoScavo().getBbox().getMaxLat()+
					record.getPosizionamentoScavo().getBbox().getMinLat())/2);
		}catch (Throwable t) {
			log.warn("Unable to evaluate centroid latitude "+t);
		}
		
		if(record.getCentroidLong()==null||record.getCentroidLong()==0)
			try {
				log.debug("Evaluating Centroid Longituted for record "+record);
				record.setCentroidLong((record.getPosizionamentoScavo().getBbox().getMaxLong()+
						record.getPosizionamentoScavo().getBbox().getMinLong())/2);
			}catch (Throwable t) {
				log.warn("Unable to evaluate centroid latitude "+t);
			}
		
		
		centroidsRow.put(DBConstants.Defaults.XCOORD_FIELD, record.getCentroidLong()+"");
		centroidsRow.put(DBConstants.Defaults.YCOORD_FIELD, record.getCentroidLat()+"");

		//Updated Schema 
		centroidsRow.put(DBConstants.Concessioni.DESCRIZIONE,record.getIntroduzione());
		centroidsRow.put(DBConstants.Concessioni.CONTENUTO,record.getDescrizioneContenuto());
		centroidsRow.put(DBConstants.Concessioni.AUTORE,asString(record.getAuthors()));
		centroidsRow.put(DBConstants.Concessioni.CONTRIBUTORE,record.getContributore());
		centroidsRow.put(DBConstants.Concessioni.TITOLARE,asString(record.getTitolari()));
		centroidsRow.put(DBConstants.Concessioni.RESPONSABILE,record.getResponsabile());
		centroidsRow.put(DBConstants.Concessioni.EDITORE,record.getEditore());
		centroidsRow.put(DBConstants.Concessioni.FINANZIAMENTO,asString(record.getFontiFinanziamento()));
		centroidsRow.put(DBConstants.Concessioni.SOGGETTO,asString(record.getSoggetto()));
		centroidsRow.put(DBConstants.Concessioni.RISORSE,asString(record.getRisorseCorrelate()));
		centroidsRow.put(DBConstants.Concessioni.DATE_SCAVO,Serialization.FULL_FORMATTER.format(record.getDataFineProgetto()));
		centroidsRow.put(DBConstants.Concessioni.DATA_ARCHIVIAZIONE,Serialization.FULL_FORMATTER.format(record.getLastUpdateTime()));
		centroidsRow.put(DBConstants.Concessioni.VERSIONE,record.getVersion());
		centroidsRow.put(DBConstants.Concessioni.LICENZA,record.getLicenzaID());
		centroidsRow.put(DBConstants.Concessioni.TITOLARE_LICENZA,asString(record.getTitolareLicenza()));
		centroidsRow.put(DBConstants.Concessioni.ACCESSO,record.getPolicy().toString());
		centroidsRow.put(DBConstants.Concessioni.PAROLE_CHIAVE,asString(record.getParoleChiaveLibere()));

		return centroidsRow;
	}
	
	
	private static String asString(Collection<?> coll) {
		if(coll==null||coll.isEmpty()) return "";
		StringBuilder builder=new StringBuilder();
		for(Object t : coll) {
			builder.append(t.toString() +",");
		}
		return builder.substring(0, builder.lastIndexOf(","));
	}
}
